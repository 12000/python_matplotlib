import matplotlib.pyplot as plt

input_values = [1, 2, 3, 4, 5]
squares = [1, 4, 9, 16, 25]
plt.plot(input_values, squares, linewidth=5)

fontSize = 24

# 设置图表标题，并给坐标轴加上标签
plt.title("Square Numbers", fontsize=fontSize)
plt.xlabel("Value", fontsize=fontSize)
plt.ylabel("Square of Value", fontsize=fontSize)

# 设置刻度标记的大小
plt.tick_params(axis='both', labelsize=14)

plt.show()